
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <chrono>
#include <future>
#include <unordered_map>

using namespace std;
using namespace cv;

// Map pour stocker les couleurs uniques
unordered_map<string, Vec3b> colorMap;

struct Region {
    int id;
    vector<Point> points;
    vector<Point> borderPoints;
    Vec3b averageColor;

    void computeAverageColor(const Mat& image) {
        Vec3f sum = Vec3f(0, 0, 0);
        for (const Point& p : points) {
            Vec3b color = image.at<Vec3b>(p);
            sum += Vec3f(color[0], color[1], color[2]);
        }
        averageColor = Vec3b(sum[0] / points.size(), sum[1] / points.size(), sum[2] / points.size());
    }

};

struct PointComparator {
    bool operator() (const cv::Point& a, const cv::Point& b) const {
        return (a.x < b.x) || (a.x == b.x && a.y < b.y);
    }
};

void showImage(Mat image, string windowName) {
    namedWindow(windowName, WINDOW_AUTOSIZE);
    imshow(windowName, image);
}

// Noyau de convolution pour la réduction de bruit
float noyauMoyenne[3][3] = {
        {1.0 / 9.0, 1.0 / 9.0, 1.0 / 9.0},
        {1.0 / 9.0, 1.0 / 9.0, 1.0 / 9.0},
        {1.0 / 9.0, 1.0 / 9.0, 1.0 / 9.0}
};

void filterImage(const Mat& image_entre, Mat& image_sortie, float noyau_filtre[3][3])
{
    int tailleNoyau = 3;
    int tailleBordure = tailleNoyau / 2;

    image_sortie.create(image_entre.size(), image_entre.type());
    for (int y = tailleBordure; y < image_entre.rows - tailleBordure; ++y)
    {
        for (int x = tailleBordure; x < image_entre.cols - tailleBordure; ++x)
        {
            Vec3f sum = Vec3f(0, 0, 0);
            for (int i = -tailleBordure; i <= tailleBordure; ++i)
            {
                for (int j = -tailleBordure; j <= tailleBordure; ++j)
                {
                    Vec3b pixel = image_entre.at<Vec3b>(y + i, x + j);
                    for (int k = 0; k < 3; ++k)
                        sum[k] += pixel[k] * noyau_filtre[i + tailleBordure][j + tailleBordure];

                }
            }
            Vec3b pixel_sortie;
            for (int k = 0; k < 3; ++k) {
                pixel_sortie[k] = saturate_cast<uchar>(sum[k]);
            }
            image_sortie.at<Vec3b>(y, x) = pixel_sortie;
        }
    }
}


// Function to divide the image into a grid and place random seeds
Mat placeRandomSeeds(const Mat& image, vector<Point>& seeds, int nbSeeds) {
    // clone the image
    Mat seededImage = image.clone();

    int gridSize = sqrt(nbSeeds);

    // initialiser le générateur de nombres aléatoires (commenter pour avoir toujours les mêmes germes)
    srand(time(NULL));

    // Dessiner le quadrillage
    for (int i = 0; i <= gridSize; ++i) {
        int x = i * (image.cols / gridSize);
        int y = i * (image.rows / gridSize);

        // Lignes verticales
        line(seededImage, Point(x, 0), Point(x, image.rows), Scalar(255, 0, 0), 1);

        // Lignes horizontales
        line(seededImage, Point(0, y), Point(image.cols, y), Scalar(255, 0, 0), 1);
    }

    // Placer les germes
    for (int i = 0; i < gridSize; ++i) {
        for (int j = 0; j < gridSize; ++j) {
            int x = rand() % (image.cols / gridSize) + j * (image.cols / gridSize);
            int y = rand() % (image.rows / gridSize) + i * (image.rows / gridSize);

            // Marquer le germe sur l'image (par exemple, avec un petit cercle)
            circle(seededImage, Point(x, y), 5, Scalar(0, 0, 255), -1);

            // Ajouter le germe à la liste des germes
            seeds.push_back(Point(x, y));
        }
    }

    return seededImage;
}

vector<Point> getNeighbors(const Point& point, const Size& imageSize) {
    vector <Point> neighbors;
    for (int dx = -1; dx <= 1; ++dx) {
        for (int dy = -1; dy <= 1; ++dy) {
            if (dx == 0 && dy == 0) continue; // Skip the center point itself

            Point neighbor = point + Point(dx, dy);
            if (neighbor.x >= 0 && neighbor.x < imageSize.width && neighbor.y >= 0 && neighbor.y < imageSize.height) {
                neighbors.push_back(neighbor);
            }
        }
    }
    return neighbors;
}

bool compareColors(const Vec3b& color1, const Vec3b& color2, int threshold) {
    int diff = 0;
    for (int i = 0; i < 3; ++i) {
        diff += abs(color1[i] - color2[i]);
    }

    return diff < threshold;
}

void regionGrowing(const Mat& image, vector<Point>& seeds, vector<Region>& regions, int threshold) {
    set<Point, PointComparator> assignedPoints; // Track points already assigned to a region

    for (const Point& seed : seeds) {
        if (assignedPoints.find(seed) != assignedPoints.end()) continue;

        Region region;
        region.id = regions.size();
        region.points.push_back(seed);
        region.averageColor = image.at<Vec3b>(seed);
        assignedPoints.insert(seed);

        vector<Point> toProcess = { seed };
        while (!toProcess.empty()) {
            vector<Point> newPoints;
            for (const Point& point : toProcess) {
                vector<Point> neighbors = getNeighbors(point, image.size());
                for (const Point& neighbor : neighbors) {
                    if (assignedPoints.find(neighbor) != assignedPoints.end()) continue;

                    if (compareColors(image.at<Vec3b>(neighbor), region.averageColor, threshold)) {
                        region.points.push_back(neighbor);
                        assignedPoints.insert(neighbor);
                        newPoints.push_back(neighbor);
                    }
                }
            }

            region.computeAverageColor(image);

            toProcess = newPoints;
        }

        regions.push_back(region);
    }
}


void colorRegionsForPictures(Mat& image, const vector<Region>& regions) {

    srand (time(NULL));

    int r, g, b;

    for (const Region& region : regions) {
        r = rand() % 255;
        g = rand() % 255;
        b = rand() % 255;
        for (const Point& point : region.points) {
            image.at<Vec3b>(point) = Vec3b(b, g, r);
        }
    }
}

string createColorKeyWithTolerance(const Vec3b& color, int tolerance) {
    // Arrondir les composantes de la couleur à la tolérance la plus proche
    int roundedR = (color[0] / tolerance) * tolerance;
    int roundedG = (color[1] / tolerance) * tolerance;
    int roundedB = (color[2] / tolerance) * tolerance;
    return to_string(roundedR) + "-" + to_string(roundedG) + "-" + to_string(roundedB);
}

void colorRegionsForPictures(Mat& image, const vector<Region>& regions, int tolerance) {
    for (const Region& region : regions) {
        string colorKey = createColorKeyWithTolerance(region.averageColor, tolerance);
        if (colorMap.find(colorKey) == colorMap.end()) {
            // Couleur non trouvée dans la map, on la crée
            int r = rand() % 255;
            int g = rand() % 255;
            int b = rand() % 255;
            colorMap[colorKey] = Vec3b(b, g, r);
        }

        Vec3b color = colorMap[colorKey];
        for (const Point& point : region.points) {
            image.at<Vec3b>(point) = color;
        }
    }
}


void updateRegionBorders(Region& region, const Mat& image, int threshold) {
    region.borderPoints.clear(); // Clear existing border points

    for (const Point& point : region.points) {
        vector<Point> neighbors = getNeighbors(point, image.size());
        for (const Point& neighbor : neighbors) {
            if (!compareColors(image.at<Vec3b>(neighbor), region.averageColor, threshold) &&
                std::find(region.points.begin(), region.points.end(), neighbor) == region.points.end()) {
                region.borderPoints.push_back(point);
                break;
            }
        }
    }
}


std::mutex mu;

void regionGrowingThread(const Mat& image, const Point& seed, vector<Region>& regions, set<Point, PointComparator>& assignedPoints, int threshold) {
    {
        std::lock_guard<std::mutex> lock(mu);
        if (assignedPoints.find(seed) != assignedPoints.end()) return;
    }

    Region region;
    region.id = regions.size();
    region.points.push_back(seed);
    region.averageColor = image.at<Vec3b>(seed);

    {
        std::lock_guard<std::mutex> lock(mu);
        // Add seed to result set
        assignedPoints.insert(seed);
        // adds newly initialized region to regions (result set)
        regions.push_back(region);
    }

    vector<Point> toProcess = { seed };
    while (!toProcess.empty()) {
        vector<Point> newPoints;
        for (const Point& point : toProcess) {
            vector<Point> neighbors = getNeighbors(point, image.size());
            for (const Point& neighbor : neighbors) {
                bool alreadyAssigned;
                {
                    std::lock_guard<std::mutex> lock(mu);
                    // Did we pass it already ?
                    alreadyAssigned = assignedPoints.find(neighbor) != assignedPoints.end();
                }
                bool similar;
                similar = compareColors(image.at<Vec3b>(neighbor), region.averageColor, threshold);
                if (!alreadyAssigned && similar) {
                    {
                        std::lock_guard<std::mutex> lock(mu);
                        // we add the neighbor to the region
                        region.points.push_back(neighbor);
                        assignedPoints.insert(neighbor);
                    }
                    newPoints.push_back(neighbor);
                }
            }
        }
        region.computeAverageColor(image);

        toProcess = newPoints;
    }
}

void regionGrowingMT(const Mat& image, vector<Point>& seeds, vector<Region>& regions, int threshold) {
    set<Point, PointComparator> assignedPoints;
    vector<std::thread> threads;

    for (const Point& seed : seeds) {
        threads.emplace_back(regionGrowingThread, std::ref(image), seed, std::ref(regions), std::ref(assignedPoints), threshold);
    }

    for (auto& thread : threads) {
        thread.join();
    }
}




int main() {

    std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
    std::chrono::duration<double> elapsed_seconds{};

    start = std::chrono::high_resolution_clock::now(); // START BENCHMARKING



    const bool MULTITHREADING = false;

    cout << "1) Region Growing sur une image" << endl;
    cout << "2) Region Growing sur une video" << endl;
    cout << "Choix : ";

    char ret;
    cin >> ret;


    switch (ret) {
        case '1': {
            cout << "Region Growing sur une image" << endl;


            //Mat img = imread("./flag_france.jpg");
            Mat img = imread("./lena.png");


            if (img.empty()) {
                cout << "Erreur de chargement de l'image." << endl;
                return -1;
            }

            showImage(img, "Image originale");


            // Application du filtre
            Mat imgFiltree;
            filterImage(img, imgFiltree, noyauMoyenne);

            vector<Point> seeds;
            Mat seededImage = placeRandomSeeds(imgFiltree, seeds, 1000);

            showImage(seededImage, "Image avec germes");

            vector<Region> regions;
            if (MULTITHREADING == false)
                regionGrowing(imgFiltree, seeds, regions, 50);
            else
                regionGrowingMT(imgFiltree, seeds, regions, 50);

            Mat regionsImage = imgFiltree.clone();
            colorRegionsForPictures(regionsImage, regions);

            showImage(regionsImage, "Image avec régions");

            cout << "Voulez-vous afficher les bordures des régions ? (y/n)" << endl;
            char c;
            cin >> c;

            if (c == 'y') {
                for (Region& region : regions) {
                    updateRegionBorders(region, imgFiltree, 10);
                    //debug
                    std::cout << "Region " << region.id << " : " << region.points.size() << " points, " << region.borderPoints.size() << " bordures" << std::endl;
                }

                Mat bordersImage = Mat::zeros(imgFiltree.size(), imgFiltree.type());

                for (const Region& region : regions) {
                    for (const Point& point : region.borderPoints) {
                        bordersImage.at<Vec3b>(point) = Vec3b(255, 255, 255);
                    }
                }

                showImage(bordersImage, "Image avec bordures");
            } else {
                cout << "Fin du programme." << endl;
            }

            // Attente d'une action de l'utilisateur
            waitKey(0);
            break;
        }
        case '2': {
            cout << "Region Growing sur une video" << endl;

            VideoCapture cap("./video_short.avi");

            if (!cap.isOpened()) {
                cout << "Erreur de chargement de la video." << endl;
                return -1;
            }

            double fps = cap.get(CAP_PROP_FPS);
            int fourcc = cv::VideoWriter::fourcc('M', 'J', 'P', 'G');
            VideoWriter writer("output_video.avi", fourcc, fps, cv::Size(2 * cap.get(cv::CAP_PROP_FRAME_WIDTH), cap.get(cv::CAP_PROP_FRAME_HEIGHT)));

            Mat frame;

            vector<Point> seeds;

            cap.read(frame);
            Mat seededImage = placeRandomSeeds(frame, seeds, 100000);

            while (cap.read(frame)) {


                // Application du filtre
                Mat imgFiltree;
                filterImage(frame, imgFiltree, noyauMoyenne);


                vector<Region> regions;
                if (MULTITHREADING == false)
                    regionGrowing(imgFiltree, seeds, regions, 30);
                else
                    regionGrowingMT(imgFiltree, seeds, regions, 30);

                Mat regionsImage = imgFiltree.clone();
                colorRegionsForPictures(regionsImage, regions, 30);


                Mat outputImage;
                hconcat(frame, regionsImage, outputImage);

                writer.write(outputImage);
                cout << "Frame " << cap.get(cv::CAP_PROP_POS_FRAMES) << endl;
            }

            cap.release();
            writer.release();

            break;
        }
        default: {
            cout << "Erreur de saisie" << endl;
            return -1;
        }

    }

    end = std::chrono::high_resolution_clock::now(); // END BENCHMARKING
    elapsed_seconds = end - start;
    std::cout << "Elapsed time (s): " << elapsed_seconds.count() << std::endl;

    return 0;
}